package fizzbuzz_test

import (
	"testing"

	"github.com/albttx/fizzbuzz/pkg/fizzbuzz"
	"github.com/stretchr/testify/require"
)

func TestFizzBuzz(t *testing.T) {
	tests := []struct {
		int1   int
		int2   int
		limit  int
		str1   string
		str2   string
		output string
	}{
		{3, 5, 10, "Fizz", "Buzz", "1,2,Fizz,4,Buzz,Fizz,7,8,Fizz,Buzz"},
	}
	for _, test := range tests {
		output := fizzbuzz.GenerateFizzBuzz(test.int1, test.int2, test.limit, test.str1, test.str2)
		println(output)
		require.Equal(t, test.output, output)
	}
}
