package fizzbuzz

import (
	"strconv"
	"strings"
)

// GenerateFizzBuzz return a fizzbuzz string
func GenerateFizzBuzz(int1, int2, limit int, str1, str2 string) string {
	res := make([]string, 0, limit)

	for i := 1; i <= limit; i++ {
		str := ""
		if i%int1 == 0 {
			str += str1
		}
		if i%int2 == 0 {
			str += str2
		}
		if str == "" {
			str += strconv.Itoa(i)
		}
		res = append(res, str)
	}

	return strings.Join(res, ",")
}
